package sso;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.founder.pojo.SysUser;
import com.founder.service.SysUserService;

public class Exam {

	@Test
	public void testDb(){
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		SysUserService sysUserService = (SysUserService) ac.getBean("sysUserService");
		List<SysUser> users = new ArrayList<SysUser>();
		try {
			users = sysUserService.getUsers();
		} catch (Exception e) {
			e.printStackTrace();
		}
		for(SysUser user:users){
			System.out.println(user.getDisplayname());
		}
	}
}
