package com.founder.dao;

import java.util.List;

import tk.mybatis.mapper.common.Mapper;

import com.founder.pojo.SysUser;

public interface SysUserDao extends Mapper<SysUser> {
	
	//根据用户名和密码查询
	public List<SysUser> geyUserByUsername(String username);
}