package com.founder.util;



import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

/**
 * 
 * 加密解密工具类
 * 
 */

public class Encrpt {
	//private static final String PASSWORD_CRYPT_KEY = "00000000";

	// private final static String DES = "DES";
	// private static final byte[] desKey;

	public final static String PASSWORD_CRYPT_KEY = "XcKMoQxk";
	/**
	 * 解密数据
	 * @param value
	 * @param key
	 */
	public static String decrypt(String value, String key) throws Exception {
		byte[] bytesrc = convertHexString(value);
		Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
		DESKeySpec desKeySpec = new DESKeySpec(key.getBytes("UTF-8"));
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
		SecretKey secretKey = keyFactory.generateSecret(desKeySpec);
		IvParameterSpec iv = new IvParameterSpec(key.getBytes("UTF-8"));
		cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);
		byte[] retByte = cipher.doFinal(bytesrc);
		return java.net.URLDecoder.decode(new String(retByte, "UTF-8"),"utf-8");
	}
	
	/**
	 * 加密数据
	 * @param value
	 * @param key
	 * @return
	 */
	public static String encrypt(String value, String key) {
		String result = "";
		try {
			value = java.net.URLEncoder.encode(value, "utf-8");
			result = toHexString(encryptByte(value, key))
					.toUpperCase();
		} catch (Exception ex) {
			ex.printStackTrace();
			return "";
		}
		return result;
	}

	public static byte[] encryptByte(String message, String key) throws Exception {
		Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");

		DESKeySpec desKeySpec = new DESKeySpec(key.getBytes("UTF-8"));

		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
		SecretKey secretKey = keyFactory.generateSecret(desKeySpec);
		IvParameterSpec iv = new IvParameterSpec(key.getBytes("UTF-8"));
		cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);

		return cipher.doFinal(message.getBytes("UTF-8"));
	}

	public static byte[] convertHexString(String ss) {
		byte digest[] = new byte[ss.length() / 2];
		for (int i = 0; i < digest.length; i++) {
			String byteString = ss.substring(2 * i, 2 * i + 2);
			int byteValue = Integer.parseInt(byteString, 16);
			digest[i] = (byte) byteValue;
		}
		return digest;
	}

	public static String toHexString(byte b[]) {
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			String plainText = Integer.toHexString(0xff & b[i]);
			if (plainText.length() < 2)
				plainText = "0" + plainText;
			hexString.append(plainText);
		}
		return hexString.toString();
	}

 
	
	
	public static String test () throws Exception {
		String key = "00000000";
		/*String value = "002586";
		String a = encrypt(value, key);
		System.out.println("加密后的数据为:" + a);
		System.out.println("解密后的数据为：" + decrypt(a, key));*/
		
		String s = "{\"policeno\":\"admin\",\"loginname\":\"sadmin\",\"idcard\":\"12345678901234568\",\"name\":\"张三\",\"orgcode\":\"340000000000\",\"orgname\":\"安徽省公安厅\",\"operateip\":\"10.10.10.10\",\"operatetime\":\"20140625010101\",\"requestsysid\":\"00000001\",\"responsesysid\":\"00000000\",\"serviceid\":\"S00000000000\"}";
		String ss = encrypt(s, key);
		return decrypt(ss, key);
	}
	
	public static void main(String[] args) throws Exception {
		String key = "founder0";
		/*String value = "002586";
		String a = encrypt(value, key);
		System.out.println("加密后的数据为:" + a);
		System.out.println("解密后的数据为：" + decrypt(a, key));*/
		System.out.println("D213742266821FC8");
		System.out.println("test:"+decrypt("D213742266821FC8",key));
		String s = "{\"policeno\":\"000001\",\"loginname\":\"sadmin\",\"idcard\":\"12345678901234568\",\"name\":\"张三\",\"orgcode\":\"340000000000\",\"orgname\":\"安徽省公安厅\",\"operateip\":\"10.10.10.10\",\"operatetime\":\"20140625010101\",\"requestsysid\":\"00000001\",\"responsesysid\":\"00000000\",\"serviceid\":\"S00000000000\"}";
		String ss = encrypt(s, key);
		System.out.println("加密使用的密码:" + key);
		System.out.println("加密前的数据为:" + s);
		System.out.println("加密后的数据为:" + ss);
		System.out.println("解密使用的密码:" + key);
		System.out.println("解密前的数据为：" + ss);
		System.out.println("解密后的数据为：" + decrypt(ss, key));
		
		 String qs="09C0DBBAD1D51D0C86A5DBDEE23D9E1679AE181B50455DCFDE40ABD1C17C877611D7D1EE6206C19DDFFA969E683E2AB5451DB0EC8AB3F5B0DB1DA8365C0B3B1C063176FD6D4940AFE5618B965597D2CC2574DF279A5DAF9D1982B2DDB649F550906ED55A4254A3BB0BD9796C16DD767E7008CECE54F8F81814C037B216A246CA3D0495E2B7288D6BA02F05299C33CC47B8DBEA73A193FCA09FC299A9592E91161BC27F10339C9AEBE3F54F974126A43496E287F39CD2935E6AAB951DF672150FBA5AB0F819E2884928E500E6D5B0359168AA0A876C35D637916D43E93BDD4B34E81687F2C390E117B285A2BE238CDE61886F0BC56542EB13C0782D2F03F1C3817A13E1AF2FD2F3DFFD5046E9BE9A3F7EB4CCD2C198C3A6135300245B9A5786CEE0133AD6E5FB8310A3D343E2434B899F47C2AFB15A40BE0E7481892C4756226011CBDBA0AAA971FED88417291ABE7E7C76B49581AD187537ED05A7E1878FC17B";
		 System.out.println("解密后的数据为：" + decrypt(qs, "XcKMoQxk"));
		
	}
}
