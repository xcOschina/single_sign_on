package com.founder.util.rsa.model;
/** 
* @author  作者  xuchen E-mail: 
* @date 创建时间：2017年12月5日 下午5:49:01 
* @version 1.0 
* @parameter  
* @since  
* @return  
*/
public class RsaModel {

	public String publicKey;
	
	public String privateKey;

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
	
	
	
}
