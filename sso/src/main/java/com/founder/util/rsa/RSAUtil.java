package com.founder.util.rsa;

import java.util.Map;

import org.junit.Test;

import com.baomidou.kisso.common.encrypt.RSA;
import com.founder.util.rsa.model.RsaModel;

/**
 * @date 创建时间：2017年12月5日 下午5:46:14
 * @version 1.0
 * @parameter
 * @since
 * @return
 * 
 * 生成秘钥对
 */
public class RSAUtil {

	public RsaModel genRSA() throws Exception {
		Map map = RSA.genKeyPair();

		String pri = RSA.getPrivateKey(map);

		String puc = RSA.getPublicKey(map);

		RsaModel rsa = new RsaModel();
		rsa.setPrivateKey(pri);
		rsa.setPublicKey(puc);

		return rsa;
	}
	
	
	@Test
	public void test() throws Exception {
		Map map = RSA.genKeyPair();
//		System.out.println(123);
		String pri = RSA.getPrivateKey(map);
		System.out.println(pri);
		String puc = RSA.getPublicKey(map);

		System.out.println(puc);

		String hdata = "helloword";
		byte[] data = hdata.getBytes();
//		System.out.println(data);

		byte[] enpr = RSA.encryptByPrivateKey(data, pri);

		byte[] enpu = RSA.encryptByPublicKey(data, puc);

		byte[] depr = RSA.decryptByPrivateKey(enpu, pri);

		byte[] depu = RSA.decryptByPublicKey(enpr, puc);
		
//		System.out.println("depr:" + new String(depr));
//		System.out.println("depu:" + new String(depu));
	}
}
