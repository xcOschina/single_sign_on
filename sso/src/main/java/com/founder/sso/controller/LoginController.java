/**
 * Copyright (c) 2011-2014, hubin (243194995@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.founder.sso.controller;

 import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.kisso.SSOConfig;
import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.SSOToken;
import com.baomidou.kisso.Token;
import com.baomidou.kisso.annotation.Action;
import com.baomidou.kisso.annotation.Login;
import com.baomidou.kisso.common.util.HttpUtil;
import com.baomidou.kisso.web.waf.request.WafRequestWrapper;
import com.founder.pojo.SysUser;
import com.founder.service.SysUserService;
import com.founder.util.Encrypt;

/**
 * <p>
 * 登录
 * </p>
 * @version 1.0.0
 */
@Controller
public class LoginController extends BaseController {
	
	@Autowired
	private SysUserService sysUserService;

	/**
	 * 登录 （注解跳过权限验证）
	 */
//	@Login(action = Action.Skip)
	@Login(action = Action.Normal)
	@RequestMapping("/login")
	public String login(Model model) {
		String returnUrl = request.getParameter(SSOConfig.getInstance().getParamReturl());
		Token token = SSOHelper.getToken(request);
		if (token == null) {
			/**
			 * 正常登录 需要过滤sql及脚本注入
			 */
			WafRequestWrapper wr = new WafRequestWrapper(request);
			
			String username = wr.getParameter("username");
			String password = wr.getParameter("password");
			
			SysUser pageUser = new SysUser();
			pageUser.setLoginname(username);
			pageUser.setPassword(password);
			
			if(username==null || "".equals(username)){
				return "login";
			}
			
			SysUser dbUser = new SysUser();
			try {
				dbUser = sysUserService.geyUserByUsername(username);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if (null!=dbUser) {
				
				if(password.equals(Encrypt.DataDecrypt(dbUser.getPassword()))){
					/*
					 * 设置登录 Cookie
					 * 最后一个参数 true 时添加 cookie 同时销毁当前 JSESSIONID 创建信任的 JSESSIONID
					 */
					SSOToken st = new SSOToken(request, dbUser.getName());
					/*加入到子系统的cookie中，并在登录子系统时将用户名写入session，比对session和cookie中用户名，正确直接进入子系统首页，错误跳转到sso登录页*/
					st.setData(username);
					SSOHelper.setSSOCookie(request, response, st, true);

					// 重定向到指定地址 returnUrl
					if (StringUtils.isEmpty(returnUrl)) {
						returnUrl = "/index";
					} else {
						returnUrl = HttpUtil.decodeURL(returnUrl);
					}
					return redirectTo(returnUrl);
				}else{
					System.out.println("密码错误");
					request.setAttribute("message", "用户名或密码错误！");
					return "login";
				}
			} else {
				if (StringUtils.isNotEmpty(returnUrl)) {
					model.addAttribute("ReturnURL", returnUrl);
				}
				return "login";
			}
		} else {
			if (StringUtils.isEmpty(returnUrl)) {
				returnUrl = "/index.html";
			}
			return redirectTo(returnUrl);
		}
	}
}