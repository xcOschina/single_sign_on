package com.founder.pojo.vo;

import com.founder.pojo.SysUser;

public class SysUserView extends SysUser{

	private String idStr;

	public String getIdStr() {
		return idStr;
	}

	public void setIdStr(String idStr) {
		this.idStr = idStr;
	}
}
