package com.founder.pojo;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "APP_USER")
public class SysUser {
    @Id
    @Column(name = "ID")
    private BigDecimal id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "GUID")
    private String guid;

    @Column(name = "CREATEDDATE")
    private Date createddate;

    @Column(name = "MODIFIEDDATE")
    private Date modifieddate;

    @Column(name = "READONLY")
    private String readonly;

    @Column(name = "NODELETE")
    private String nodelete;

    @Column(name = "LOGINNAME")
    private String loginname;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "LEVELID")
    private BigDecimal levelid;

    @Column(name = "DUTYLEVELID")
    private BigDecimal dutylevelid;

    @Column(name = "DISABLED")
    private String disabled;

    @Column(name = "NODISABLE")
    private String nodisable;

    @Column(name = "LASTLOGIN")
    private Date lastlogin;

    @Column(name = "LAST_VISITED_PAGE")
    private BigDecimal lastVisitedPage;

    @Column(name = "SORTORDER")
    private BigDecimal sortorder;

    @Column(name = "PRIORITY")
    private Long priority;

    @Column(name = "NTACCOUT")
    private String ntaccout;

    @Column(name = "RECORDTYPE")
    private Long recordtype;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "MOBILEPHONE")
    private String mobilephone;

    @Column(name = "LENDCOPIES")
    private Long lendcopies;

    @Column(name = "RECORDNUMPERPAGE")
    private Long recordnumperpage;

    @Column(name = "FRAMEPERPAGE")
    private Long frameperpage;

    @Column(name = "VERIFIED")
    private Long verified;

    @Column(name = "ISABSENT")
    private Integer isabsent;

    @Column(name = "SUBSTITUTE")
    private Long substitute;

    @Column(name = "SEXY")
    private BigDecimal sexy;

    @Column(name = "USERTYPE")
    private Long usertype;

    @Column(name = "GROUPCODE")
    private String groupcode;

    @Column(name = "PTYPE")
    private Long ptype;

    @Column(name = "SALARY")
    private Integer salary;

    @Column(name = "ORGID")
    private String orgid;

    @Column(name = "DONGXI")
    private String dongxi;

    @Column(name = "GROUPLEVEL")
    private Long grouplevel;

    @Column(name = "SHUXING1")
    private Long shuxing1;

    @Column(name = "ASD")
    private String asd;

    @Column(name = "SHUXING3")
    private Integer shuxing3;

    @Column(name = "SHUXING33")
    private Integer shuxing33;

    @Column(name = "SHUXING")
    private Integer shuxing;

    @Column(name = "SDF")
    private Integer sdf;

    @Column(name = "LABEL")
    private String label;

    @Column(name = "SS")
    private Integer ss;

    @Column(name = "ISRO")
    private Integer isro;

    @Column(name = "FATHERID")
    private Long fatherid;

    @Column(name = "DISPLAYNAME")
    private String displayname;

    @Column(name = "NTLMPASSWORD")
    private String ntlmpassword;

    /**
     * @return ID
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(BigDecimal id) {
        this.id = id;
    }

    /**
     * @return NAME
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * @return DESCRIPTION
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * @return GUID
     */
    public String getGuid() {
        return guid;
    }

    /**
     * @param guid
     */
    public void setGuid(String guid) {
        this.guid = guid == null ? null : guid.trim();
    }

    /**
     * @return CREATEDDATE
     */
    public Date getCreateddate() {
        return createddate;
    }

    /**
     * @param createddate
     */
    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }

    /**
     * @return MODIFIEDDATE
     */
    public Date getModifieddate() {
        return modifieddate;
    }

    /**
     * @param modifieddate
     */
    public void setModifieddate(Date modifieddate) {
        this.modifieddate = modifieddate;
    }

    /**
     * @return READONLY
     */
    public String getReadonly() {
        return readonly;
    }

    /**
     * @param readonly
     */
    public void setReadonly(String readonly) {
        this.readonly = readonly == null ? null : readonly.trim();
    }

    /**
     * @return NODELETE
     */
    public String getNodelete() {
        return nodelete;
    }

    /**
     * @param nodelete
     */
    public void setNodelete(String nodelete) {
        this.nodelete = nodelete == null ? null : nodelete.trim();
    }

    /**
     * @return LOGINNAME
     */
    public String getLoginname() {
        return loginname;
    }

    /**
     * @param loginname
     */
    public void setLoginname(String loginname) {
        this.loginname = loginname == null ? null : loginname.trim();
    }

    /**
     * @return PASSWORD
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * @return EMAIL
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     */
    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    /**
     * @return LEVELID
     */
    public BigDecimal getLevelid() {
        return levelid;
    }

    /**
     * @param levelid
     */
    public void setLevelid(BigDecimal levelid) {
        this.levelid = levelid;
    }

    /**
     * @return DUTYLEVELID
     */
    public BigDecimal getDutylevelid() {
        return dutylevelid;
    }

    /**
     * @param dutylevelid
     */
    public void setDutylevelid(BigDecimal dutylevelid) {
        this.dutylevelid = dutylevelid;
    }

    /**
     * @return DISABLED
     */
    public String getDisabled() {
        return disabled;
    }

    /**
     * @param disabled
     */
    public void setDisabled(String disabled) {
        this.disabled = disabled == null ? null : disabled.trim();
    }

    /**
     * @return NODISABLE
     */
    public String getNodisable() {
        return nodisable;
    }

    /**
     * @param nodisable
     */
    public void setNodisable(String nodisable) {
        this.nodisable = nodisable == null ? null : nodisable.trim();
    }

    /**
     * @return LASTLOGIN
     */
    public Date getLastlogin() {
        return lastlogin;
    }

    /**
     * @param lastlogin
     */
    public void setLastlogin(Date lastlogin) {
        this.lastlogin = lastlogin;
    }

    /**
     * @return LAST_VISITED_PAGE
     */
    public BigDecimal getLastVisitedPage() {
        return lastVisitedPage;
    }

    /**
     * @param lastVisitedPage
     */
    public void setLastVisitedPage(BigDecimal lastVisitedPage) {
        this.lastVisitedPage = lastVisitedPage;
    }

    /**
     * @return SORTORDER
     */
    public BigDecimal getSortorder() {
        return sortorder;
    }

    /**
     * @param sortorder
     */
    public void setSortorder(BigDecimal sortorder) {
        this.sortorder = sortorder;
    }

    /**
     * @return PRIORITY
     */
    public Long getPriority() {
        return priority;
    }

    /**
     * @param priority
     */
    public void setPriority(Long priority) {
        this.priority = priority;
    }

    /**
     * @return NTACCOUT
     */
    public String getNtaccout() {
        return ntaccout;
    }

    /**
     * @param ntaccout
     */
    public void setNtaccout(String ntaccout) {
        this.ntaccout = ntaccout == null ? null : ntaccout.trim();
    }

    /**
     * @return RECORDTYPE
     */
    public Long getRecordtype() {
        return recordtype;
    }

    /**
     * @param recordtype
     */
    public void setRecordtype(Long recordtype) {
        this.recordtype = recordtype;
    }

    /**
     * @return PHONE
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    /**
     * @return MOBILEPHONE
     */
    public String getMobilephone() {
        return mobilephone;
    }

    /**
     * @param mobilephone
     */
    public void setMobilephone(String mobilephone) {
        this.mobilephone = mobilephone == null ? null : mobilephone.trim();
    }

    /**
     * @return LENDCOPIES
     */
    public Long getLendcopies() {
        return lendcopies;
    }

    /**
     * @param lendcopies
     */
    public void setLendcopies(Long lendcopies) {
        this.lendcopies = lendcopies;
    }

    /**
     * @return RECORDNUMPERPAGE
     */
    public Long getRecordnumperpage() {
        return recordnumperpage;
    }

    /**
     * @param recordnumperpage
     */
    public void setRecordnumperpage(Long recordnumperpage) {
        this.recordnumperpage = recordnumperpage;
    }

    /**
     * @return FRAMEPERPAGE
     */
    public Long getFrameperpage() {
        return frameperpage;
    }

    /**
     * @param frameperpage
     */
    public void setFrameperpage(Long frameperpage) {
        this.frameperpage = frameperpage;
    }

    /**
     * @return VERIFIED
     */
    public Long getVerified() {
        return verified;
    }

    /**
     * @param verified
     */
    public void setVerified(Long verified) {
        this.verified = verified;
    }

    /**
     * @return ISABSENT
     */
    public Integer getIsabsent() {
        return isabsent;
    }

    /**
     * @param isabsent
     */
    public void setIsabsent(Integer isabsent) {
        this.isabsent = isabsent;
    }

    /**
     * @return SUBSTITUTE
     */
    public Long getSubstitute() {
        return substitute;
    }

    /**
     * @param substitute
     */
    public void setSubstitute(Long substitute) {
        this.substitute = substitute;
    }

    /**
     * @return SEXY
     */
    public BigDecimal getSexy() {
        return sexy;
    }

    /**
     * @param sexy
     */
    public void setSexy(BigDecimal sexy) {
        this.sexy = sexy;
    }

    /**
     * @return USERTYPE
     */
    public Long getUsertype() {
        return usertype;
    }

    /**
     * @param usertype
     */
    public void setUsertype(Long usertype) {
        this.usertype = usertype;
    }

    /**
     * @return GROUPCODE
     */
    public String getGroupcode() {
        return groupcode;
    }

    /**
     * @param groupcode
     */
    public void setGroupcode(String groupcode) {
        this.groupcode = groupcode == null ? null : groupcode.trim();
    }

    /**
     * @return PTYPE
     */
    public Long getPtype() {
        return ptype;
    }

    /**
     * @param ptype
     */
    public void setPtype(Long ptype) {
        this.ptype = ptype;
    }

    /**
     * @return SALARY
     */
    public Integer getSalary() {
        return salary;
    }

    /**
     * @param salary
     */
    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    /**
     * @return ORGID
     */
    public String getOrgid() {
        return orgid;
    }

    /**
     * @param orgid
     */
    public void setOrgid(String orgid) {
        this.orgid = orgid == null ? null : orgid.trim();
    }

    /**
     * @return DONGXI
     */
    public String getDongxi() {
        return dongxi;
    }

    /**
     * @param dongxi
     */
    public void setDongxi(String dongxi) {
        this.dongxi = dongxi == null ? null : dongxi.trim();
    }

    /**
     * @return GROUPLEVEL
     */
    public Long getGrouplevel() {
        return grouplevel;
    }

    /**
     * @param grouplevel
     */
    public void setGrouplevel(Long grouplevel) {
        this.grouplevel = grouplevel;
    }

    /**
     * @return SHUXING1
     */
    public Long getShuxing1() {
        return shuxing1;
    }

    /**
     * @param shuxing1
     */
    public void setShuxing1(Long shuxing1) {
        this.shuxing1 = shuxing1;
    }

    /**
     * @return ASD
     */
    public String getAsd() {
        return asd;
    }

    /**
     * @param asd
     */
    public void setAsd(String asd) {
        this.asd = asd == null ? null : asd.trim();
    }

    /**
     * @return SHUXING3
     */
    public Integer getShuxing3() {
        return shuxing3;
    }

    /**
     * @param shuxing3
     */
    public void setShuxing3(Integer shuxing3) {
        this.shuxing3 = shuxing3;
    }

    /**
     * @return SHUXING33
     */
    public Integer getShuxing33() {
        return shuxing33;
    }

    /**
     * @param shuxing33
     */
    public void setShuxing33(Integer shuxing33) {
        this.shuxing33 = shuxing33;
    }

    /**
     * @return SHUXING
     */
    public Integer getShuxing() {
        return shuxing;
    }

    /**
     * @param shuxing
     */
    public void setShuxing(Integer shuxing) {
        this.shuxing = shuxing;
    }

    /**
     * @return SDF
     */
    public Integer getSdf() {
        return sdf;
    }

    /**
     * @param sdf
     */
    public void setSdf(Integer sdf) {
        this.sdf = sdf;
    }

    /**
     * @return LABEL
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label
     */
    public void setLabel(String label) {
        this.label = label == null ? null : label.trim();
    }

    /**
     * @return SS
     */
    public Integer getSs() {
        return ss;
    }

    /**
     * @param ss
     */
    public void setSs(Integer ss) {
        this.ss = ss;
    }

    /**
     * @return ISRO
     */
    public Integer getIsro() {
        return isro;
    }

    /**
     * @param isro
     */
    public void setIsro(Integer isro) {
        this.isro = isro;
    }

    /**
     * @return FATHERID
     */
    public Long getFatherid() {
        return fatherid;
    }

    /**
     * @param fatherid
     */
    public void setFatherid(Long fatherid) {
        this.fatherid = fatherid;
    }

    /**
     * @return DISPLAYNAME
     */
    public String getDisplayname() {
        return displayname;
    }

    /**
     * @param displayname
     */
    public void setDisplayname(String displayname) {
        this.displayname = displayname == null ? null : displayname.trim();
    }

    /**
     * @return NTLMPASSWORD
     */
    public String getNtlmpassword() {
        return ntlmpassword;
    }

    /**
     * @param ntlmpassword
     */
    public void setNtlmpassword(String ntlmpassword) {
        this.ntlmpassword = ntlmpassword == null ? null : ntlmpassword.trim();
    }
}