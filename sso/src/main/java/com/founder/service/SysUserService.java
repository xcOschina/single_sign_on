package com.founder.service;

import java.util.List;

import tk.mybatis.mapper.common.Mapper;

import com.founder.pojo.SysUser;

public interface SysUserService extends Mapper<SysUser>{

	//获取所有用户
	public List<SysUser> getUsers() throws Exception;
	
	//根据用户名查询
	public SysUser geyUserByUsername(String username) throws Exception;
}