package com.founder.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.founder.dao.SysUserDao;
import com.founder.pojo.SysUser;
import com.founder.service.BaseService;
import com.founder.service.SysUserService;

@Service("sysUserService")
public class SysUserServiceImpl extends BaseService<SysUser> implements SysUserService {

	@Autowired
	private SysUserDao userDao;
	
	//获取所有用户
	public List<SysUser> getUsers() throws Exception{
		return userDao.selectAll();
	}

	public SysUser geyUserByUsername(String username) throws Exception {
		
		List<SysUser> users = userDao.geyUserByUsername(username);
		if(users.size()==0){
			return null;
		}
		return users.get(0);
	}
}
