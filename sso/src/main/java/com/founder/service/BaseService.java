package com.founder.service;

 
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;

import tk.mybatis.mapper.common.Mapper;

public class BaseService<T> implements Mapper<T> {
	@Autowired
 	protected Mapper<T> mapper;
	@Override
	public T selectOne(T record) {
		// TODO 自动生成的方法存根
		return mapper.selectOne(record);
	}
 

	@Override
	public List<T> selectAll() {
		// TODO 自动生成的方法存根
		return mapper.selectAll();
	}


	@Override
	public List<T> select(T record) {
		// TODO 自动生成的方法存根
		return mapper.select(record);
	}


	@Override
	public int selectCount(T record) {
		// TODO 自动生成的方法存根
		return mapper.selectCount(record);
	}


	@Override
	public T selectByPrimaryKey(Object key) {
		// TODO 自动生成的方法存根
		return mapper.selectByPrimaryKey(key);
	}


	@Override
	public boolean existsWithPrimaryKey(Object key) {
		// TODO 自动生成的方法存根
		return mapper.existsWithPrimaryKey(key);
	}


	@Override
	public int insert(T record) {
		// TODO 自动生成的方法存根
		return mapper.insert(record);
	}


	@Override
	public int insertSelective(T record) {
		// TODO 自动生成的方法存根
		return mapper.insertSelective(record);
	}


	@Override
	public int updateByPrimaryKey(T record) {
		// TODO 自动生成的方法存根
		return mapper.updateByPrimaryKey(record);
	}


	@Override
	public int updateByPrimaryKeySelective(T record) {
		// TODO 自动生成的方法存根
		return mapper.updateByPrimaryKeySelective(record);
	}


	@Override
	public int delete(T record) {
		// TODO 自动生成的方法存根
		return mapper.delete(record);
	}


	@Override
	public int deleteByPrimaryKey(Object key) {
		// TODO 自动生成的方法存根
		return mapper.deleteByPrimaryKey(key);
	}


	@Override
	public List<T> selectByExample(Object example) {
		// TODO 自动生成的方法存根
		return mapper.selectByExample(example);
	}


	@Override
	public int selectCountByExample(Object example) {
		// TODO 自动生成的方法存根
		return mapper.selectCountByExample(example);
	}


	@Override
	public int deleteByExample(Object example) {
		// TODO 自动生成的方法存根
		return mapper.deleteByExample(example);
	}


	@Override
	public int updateByExample(T record, Object example) {
		// TODO 自动生成的方法存根
		return mapper.updateByExample(record, example);
	}


	@Override
	public int updateByExampleSelective(T record, Object example) {
		// TODO 自动生成的方法存根
		return mapper.updateByExampleSelective(record, example);
	}


	@Override
	public List<T> selectByExampleAndRowBounds(Object example, RowBounds rowBounds) {
		// TODO 自动生成的方法存根
		return mapper.selectByExampleAndRowBounds(example, rowBounds);
	}


	@Override
	public List<T> selectByRowBounds(T record, RowBounds rowBounds) {
		// TODO 自动生成的方法存根
		return mapper.selectByRowBounds(record, rowBounds);
	}

	 
	 
}
 
