/**
 * 文件名：Mbg.java
 * 版权：Copyright by www.bjleisen.com
 * 描述�?
 * 修改人：zhangziqi
 * 修改时间�?2016�?10�?27�?
 * 修改内容�?
 */

package com.founder.creater;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

public class Creating {

	public static void main(String[] args) {
		try {
			List<String> warnings = new ArrayList<String>();
			boolean overwrite = true;
			ConfigurationParser cp = new ConfigurationParser(warnings);
			Configuration config = cp.parseConfiguration(Creating.class.getResourceAsStream("generator.xml"));
			DefaultShellCallback callback = new DefaultShellCallback(overwrite);
			MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
			myBatisGenerator.generate(null);
			System.out.println("生成成功");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("生成失败");
		}
	}
}
