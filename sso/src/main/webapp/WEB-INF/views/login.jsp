<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>云平台-登录</title>
    <link rel="stylesheet" type="text/css" href="static/css/login.css">
    <link rel="stylesheet" type="text/css" href="static/plugins/bootstrap-3.3.7-dist/css/bootstrap.min.css">
</head>

<body>
    <div class="bg_top">
        <!-- <div class="top_tit">
        	<span>安徽公安云平台</span>
        	<a href="">登录</a>
        </div> -->
    </div>
    <div class="bg_center">
        <div class="login_box">
            <div class="login_tit">
                <h1>安徽公安职业学院<br><span class="sub_tit">云服务平台</span></h1>
                <!-- <p>请输入用户名和密码</p> -->
            </div>
            <div class="login_con">
                <form method="post" action="login">
                    <div class="form-group">
                        <input type="text"  name="username" class="form-control" placeholder="请输入用户名">
                    </div>
                    <div class="form-group">
                        <input type="password"  name="password" class="form-control" placeholder="请输入密码"><span class="password_err">${message}</span>
                    </div>
                    
                    	<input type="hidden" name="ReturnURL" value="${ReturnURL }"/>
                    <div class="checkbox remer_psd">
                        <label>
                            <input type="checkbox"> 记住密码
                        </label>
                        <a class="modify_psd" href="psd.html">
                           修改密码
                        </a>
                    </div>
                    <button type="submit" class="btn btn-primary login_btn"  >登录</button>
                    <!-- <div class="modify_psd">
                        <a href="psd.html" class="txt">
                            修改密码
                        </a>
                    </div> -->
                </form>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript" src="static/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="static/plugins/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="static/plugins/jquery.placeholder.min.js"></script>
<script>
 $(function() { $('.form-group input').placeholder();});

</script>
</html>