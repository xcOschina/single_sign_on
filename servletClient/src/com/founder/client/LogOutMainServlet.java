package com.founder.client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.Token;

public class LogOutMainServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -188797775545430562L;

	/**
	 * 公用退出
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Token tk = SSOHelper.getToken(request);
		SSOHelper.clearLogin(request, response);
		
		response.sendRedirect("http://172.29.189.22:8080/sso/success");
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		this.doGet(request, response);
	}
}