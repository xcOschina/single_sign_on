package com.founder.client;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.Token;

public class LogOutPublicServlet extends HttpServlet {

	/**
	 * 公共退出
	 */
	private static final long serialVersionUID = -1245212029744640519L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		/**
		 * <p>
		 * SSO 退出，清空退出状态即可
		 * </p>
		 * 
		 * <p>
		 * 子系统退出 SSOHelper.logout(request, response); 注意 sso.properties 包含 退出到
		 * SSO 的地址 ， 属性 sso.logout.url 的配置
		 * </p>
		 */
		Token tk = SSOHelper.getToken(request);
		SSOHelper.clearLogin(request, response);
		AjaxHelper.outPrint(response, "success", "Utf-8");
		 
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}
}